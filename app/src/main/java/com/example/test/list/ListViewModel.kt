package com.example.test.list

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.test.data.Devices
import com.example.test.data.database.ItemDao
import com.example.test.data.database.ItemEntity
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    private val client: HttpClient,
    private val dao: ItemDao
) : ViewModel() {
    private val BASE_URL = "https://veramobile.mios.com/test_android/items.test"

    private val _uiState = MutableStateFlow(ListState())
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            try {
                if (dao.get().isEmpty()) {
                    getData()
                } else {
                    _uiState.update { state ->
                        state.copy(list = dao.get())
                    }
                }
            } catch (e: Exception) {
                Log.d("TAG", ": $e")
            }
        }
    }

    fun deleteItem(itemEntity: ItemEntity) {
        viewModelScope.launch {
            dao.delete(itemEntity)
            _uiState.update { state ->
                state.copy(list = dao.get())
            }
        }
    }

    fun reset() {
        viewModelScope.launch {
            dao.get().forEach {
                dao.delete(it)
            }
            getData()
        }
    }

    private suspend fun getData() {
        val text = client.get(BASE_URL).bodyAsText()
        text.byteInputStream().bufferedReader().use {
            it.readLine()
        }

        val list = Gson()
            .fromJson(text, Devices::class.java)
            .deviceList

        list.forEach {
            dao.insert(
                item = ItemEntity(
                    sn = "SN ${it.pkDevice}",
                    macAddress = it.macAddress.orEmpty(),
                    firmware = it.firmware.orEmpty(),
                    platform = it.platform.orEmpty()
                )
            )
            _uiState.update { state ->
                state.copy(list = dao.get())
            }
        }
    }
}