package com.example.test.detail

import com.example.test.data.database.ItemEntity

data class DetailState(
    val item: ItemEntity? = null
)