package com.example.test.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ItemDao {

    @Query("SELECT * FROM item")
    suspend fun get(): List<ItemEntity>

    @Insert
    suspend fun insert(item: ItemEntity)

    @Update(entity = ItemEntity::class)
    suspend fun update(item: ItemEntity)

    @Delete
    suspend fun delete(item: ItemEntity)
}