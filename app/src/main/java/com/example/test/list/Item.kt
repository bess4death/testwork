package com.example.test.list

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.KeyboardArrowUp
import androidx.compose.material3.Card
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.test.core.PlatformKey
import com.example.test.data.database.ItemEntity

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Item(item: ItemEntity, deleteItem: () -> Unit, openDetail: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(72.dp)
            .combinedClickable(onClick = {openDetail()}, onLongClick = {deleteItem()}),
    ) {
        Card(
            modifier = Modifier
                .align(Alignment.CenterStart)
                .padding(start = 16.dp),
        ) {
            Image(
                modifier = Modifier.size(54.dp),
                painter = painterResource(id = PlatformKey.getImageByKey(item.platform)),
                contentDescription = ""
            )
        }
        Column(
            modifier = Modifier
                .padding(start = 88.dp)
                .align(Alignment.CenterStart)
        ) {
            Text(text = "Home number ${item.uid}", style = MaterialTheme.typography.headlineSmall)
            Text(text = item.sn, style = MaterialTheme.typography.bodyMedium)
        }
        Icon(
            modifier = Modifier
                .size(48.dp)
                .padding(end = 16.dp)
                .rotate(90F)

                .align(Alignment.CenterEnd),
            imageVector = Icons.Rounded.KeyboardArrowUp,
            contentDescription = ""
        )
        HorizontalDivider(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
        )
    }
}