package com.example.test.detail

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.test.R
import com.example.test.core.Header
import com.example.test.core.PlatformKey

@Composable
fun DetailScreen(
    viewModel: DetailViewModel = hiltViewModel(),
    id: Int,
) {
    val uiState by viewModel.uiState.collectAsState()
    viewModel.init(id)
    Log.d("TAG", "DetailScreen: ${uiState.item}")

    Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        Header()
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colorScheme.surface)
                .padding(16.dp)
                .weight(1f),
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Card {
                    Image(
                        modifier = Modifier.size(72.dp),
                        painter = painterResource(id = PlatformKey.getImageByKey(uiState.item?.platform)),
                        contentDescription = ""
                    )
                }
                Text(
                    modifier = Modifier.padding(start = 24.dp),
                    text = "Home number ${uiState.item?.uid}",
                    style = MaterialTheme.typography.headlineSmall
                )
            }
            Column {
                Text(
                    text = uiState.item?.sn.orEmpty(),
                    style = MaterialTheme.typography.headlineSmall
                )
                Text(
                    text = uiState.item?.macAddress.orEmpty(),
                    style = MaterialTheme.typography.headlineSmall
                )
            }
            Column {
                Text(
                    text = uiState.item?.firmware.orEmpty(),
                    style = MaterialTheme.typography.headlineSmall
                )
                Text(
                    text = PlatformKey.getNameByKey(uiState.item?.platform),
                    style = MaterialTheme.typography.headlineSmall
                )
            }
        }
    }
}