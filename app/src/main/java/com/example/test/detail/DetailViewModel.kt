package com.example.test.detail

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.test.data.database.ItemDao
import com.example.test.list.ListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val dao: ItemDao
) : ViewModel() {
    private val _uiState = MutableStateFlow(DetailState())
    val uiState = _uiState.asStateFlow()

    fun init(uid: Int) {
        viewModelScope.launch {
            val item = dao.get().find { it.uid == uid }
            _uiState.update { state ->
                state.copy(item = item)
            }
        }
    }

}