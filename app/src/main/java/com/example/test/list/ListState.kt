package com.example.test.list

import com.example.test.data.database.ItemEntity

data class ListState(
    val list: List<ItemEntity> = listOf(),
)