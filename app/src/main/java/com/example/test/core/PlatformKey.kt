package com.example.test.core

import com.example.test.R

object PlatformKey {
    fun getImageByKey(key: String?): Int =
        when (key) {
            "Sercomm G450" -> R.drawable.vera_plus_big
            "Sercomm G550" -> R.drawable.vera_secure_big
            "MiCasaVerde VeraLite", "Sercomm NA900", "Sercomm NA301", "Sercomm NA930", null -> R.drawable.vera_secure_big
            else -> R.drawable.vera_secure_big
        }

    fun getNameByKey(key: String?): String =
        when (key) {
            "Sercomm G450" -> "Vera Plus"
            "Sercomm G550" -> "Vera Secure"
            "MiCasaVerde VeraLite", "Sercomm NA900", "Sercomm NA301", "Sercomm NA930", null -> "Vera Edge"
            else -> "Vera Edge"
        }
}