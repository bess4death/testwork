package com.example.test.data

import com.google.gson.annotations.SerializedName

data class Device(
    @SerializedName("PK_Device")
    val pkDevice: Long? = null,
    @SerializedName("MacAddress")
    val macAddress: String? = null,
    @SerializedName("PK_DeviceType")
    val pkDeviceType: Int? = null,
    @SerializedName("PK_DeviceSubType")
    val pkDeviceSubType: Int? = null,
    @SerializedName("PK_Account")
    val pkAccount: Long? = null,
    @SerializedName("Firmware")
    val firmware: String? = null,
    @SerializedName("Server_Device")
    val serverDevice: String? = null,
    @SerializedName("Server_Event")
    val serverEvent: String? = null,
    @SerializedName("Server_Account")
    val serverAccount: String? = null,
    @SerializedName("InternalIP")
    val internalIp: String? = null,
    @SerializedName("LastAliveReported")
    val lastAliveReported: String? = null,
    @SerializedName("Platform")
    val platform: String? = null,
)