package com.example.test.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Constants.databaseName)
data class ItemEntity (
    @PrimaryKey(autoGenerate = true)
    val uid: Int = 0,
    @ColumnInfo(name = "sn")
    val sn: String,
    @ColumnInfo(name = "macAddress")
    val macAddress: String,
    @ColumnInfo(name = "firmware")
    val firmware: String,
    @ColumnInfo(name = "platform")
    val platform: String,
)