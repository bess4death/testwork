package com.example.test.list

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.test.core.Header
import com.example.test.data.database.ItemEntity

@Composable
fun ListScreen(
    viewModel: ListViewModel = hiltViewModel(),
    openDetail: (ItemEntity) -> Unit
) {
    val uiState by viewModel.uiState.collectAsState()
    var isShowDialog by remember { mutableStateOf(false) }
    var item by remember { mutableStateOf<ItemEntity?>(null) }

    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.surfaceContainer)
        ) {
            Header()
            LazyColumn(
                modifier = Modifier
                    .weight(1f)
                    .background(MaterialTheme.colorScheme.surface)
            ) {
                items(uiState.list, key = { it.uid }) {
                    Item(item = it, deleteItem = {
                        item = it
                        isShowDialog = true
                    }) {
                        openDetail(it)
                    }
                }
            }
        }
        Button(modifier = Modifier
            .align(Alignment.TopCenter)
            .padding(32.dp),
            onClick = { viewModel.reset() }) {
            Text(text = "Reset")
        }
    }

    if (isShowDialog) {
        Dialog(onDismissRequest = { isShowDialog = false }) {
            Card(modifier = Modifier.fillMaxWidth()) {
                Text(modifier = Modifier.padding(16.dp), text = "Do you want delete this item?")
                Row(
                    modifier = Modifier
                        .align(Alignment.End)
                        .padding(end = 16.dp, bottom = 16.dp)
                ) {
                    Text(modifier = Modifier.clickable {
                        isShowDialog = false
                    }, text = "No")
                    Spacer(modifier = Modifier.size(8.dp))
                    Text(modifier = Modifier.clickable {
                        item?.let { viewModel.deleteItem(itemEntity = it) }
                        isShowDialog = false
                    }, text = "Yes")
                }
            }
        }
    }
}