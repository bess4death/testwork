package com.example.test.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.test.detail.DetailScreen
import com.example.test.list.ListScreen

private const val DEVICE = "DEVICE"

sealed class Navigation(val route: String) {
    data object List : Navigation("list")
    data object Detail : Navigation("detail")
}

@Composable
fun MainNavHost() {
    val controller = rememberNavController()
    NavHost(navController = controller, startDestination = Navigation.List.route) {
        composable(Navigation.List.route) {
            ListScreen {
                controller.navigate(Navigation.Detail.route + "/${it.uid}")
            }
        }
        composable(Navigation.Detail.route + "/{$DEVICE}", arguments = listOf(
            navArgument(DEVICE) { type = NavType.IntType }
        )) {
            val id = it.arguments?.getInt(DEVICE) ?: 0
            DetailScreen(id = id)
        }
    }
}

