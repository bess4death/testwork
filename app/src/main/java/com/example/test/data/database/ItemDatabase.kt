package com.example.test.data.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ItemEntity::class], version = 4)
internal abstract class ItemDatabase: RoomDatabase() {
    abstract fun getDao(): ItemDao
}