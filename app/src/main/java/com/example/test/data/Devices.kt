package com.example.test.data

import com.google.gson.annotations.SerializedName

data class Devices(
    @SerializedName("Devices")
    val deviceList: List<Device>,
)