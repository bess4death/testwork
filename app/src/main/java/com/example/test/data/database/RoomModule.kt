package com.example.test.data.database

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
internal object RoomModule {
    @Provides
    @Singleton
    fun provideProfileDatabase(@ApplicationContext appContext: Context): ItemDatabase =
        Room.databaseBuilder(appContext, ItemDatabase::class.java, Constants.databaseName)
            .fallbackToDestructiveMigration().build()

    @Provides
    fun provideProfileDao(database: ItemDatabase): ItemDao = database.getDao()
}